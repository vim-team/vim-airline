vim-airline (0.11-2) unstable; urgency=low

  * Remove strict dependency on vim-addon-manager(1) and migrate
    to dh_vim-addon:
    - Remove vim-addon-manager from Depends
    - Remove postinst, preinst, prerm maintainer scripts or their parts
      calling VAM
    - Add dh-sequence-vim-addon to Build-Depends
    - Add substvar ${vim-addon:Depends} to Depends
    - Create file debian/vim-airline.vim-addon in the source directory
    - Symlink debian/vim-airline.neovim-addon if your plugin works in NeoVim
    - Thanks to Nicholas Guriev for this debdiff (Closes: 1015934)
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse
  * Update standards version to 4.6.1
  * Upgrade to debhelper-compat (=13)

 -- Jonathan Carter <jcc@debian.org>  Tue, 02 Aug 2022 10:47:01 +0200

vim-airline (0.11-1) unstable; urgency=medium

  * New upstream release
  * Update standards version to 4.4.1
  * Upgrade to debhelper-compat (=12)
  * Update copyright years
  * Update debian/vim-airline.yaml with new plugins

 -- Jonathan Carter <jcc@debian.org>  Tue, 12 Nov 2019 09:11:59 +0000

vim-airline (0.10-1) unstable; urgency=medium

  * New upstream release
  * Update copyright file
  * Update standards version to 4.2.1
  * Update debian/vim-airline.yaml with new plugins

 -- Jonathan Carter <jcc@debian.org>  Tue, 18 Dec 2018 17:00:12 +0200

vim-airline (0.9-3) unstable; urgency=medium

  * Version bump (no changes)

 -- Jonathan Carter <jcc@debian.org>  Thu, 22 Nov 2018 23:15:23 +0200

vim-airline (0.9-2) unstable; urgency=medium

  * Move VCS to Debian vim team (Closes: 894803)
  * Update standards version to 4.1.4

 -- Jonathan Carter <jcc@debian.org>  Thu, 05 Apr 2018 15:01:59 +0200

vim-airline (0.9-1) unstable; urgency=medium

  * New upstream release
  * Update copyright information
  * New maintainer e-mail address
  * Update compat to level 11
  * Update standards version to 4.1.3
  * Move VCS to salsa.debian.net
  * Update debian/vim-airline.yaml with new file list

 -- Jonathan Carter <jcc@debian.org>  Thu, 25 Jan 2018 11:36:02 +0200

vim-airline (0.8-1) unstable; urgency=medium

  * Initial release (Closes: #868044)

 -- Jonathan Carter <jcarter@linux.com>  Tue, 11 Jul 2017 12:55:20 +0200
